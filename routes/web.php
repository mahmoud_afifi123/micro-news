<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home_page');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('article/{id}', 'ArticlesController@show');
Route::get('logout', 'Auth\LoginController@logout');
//admin routes
Route::group(['prefix'=>'admin','middleware'=>'auth'], function(){
    Route::get('/', 'AdminController@index');
    //articles routes
    Route::resource('articles', 'ArticlesController');
    Route::get('/articles/{id}/delete', 'ArticlesController@destroy');
    //categories routes
    Route::resource('categories', 'CategoriesController');
    Route::get('/categories/{id}/delete', 'CategoriesController@destroy');

});
