@include('admin.nav')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h2>Featured Articles</h2>
                    @if(isset($articles['featured']))
                        @foreach ($articles['featured']->chunk(2) as $chunkedArticle)
                            <div class="row">
                                @foreach($chunkedArticle as $article)
                                    @if($article->featured == 'featured')
                                        <div class="col-md-4">
                                            <div class="card">
                                                <img class="card-img-top" alt="Bootstrap Thumbnail First"
                                                     width="200" src="{{url("images/$article->image")}}"/>
                                                <div class="card-block">
                                                    <h5 class="card-title">
                                                        {{$article->title}}
                                                    </h5>
                                                    <p class="card-text">
                                                        category: {{$article->categories->name}}
                                                    </p>
                                                    <p>
                                                        <a class="btn btn-primary" href="{{url("article/$article->id/")
                                }}">Details</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-6">
                    <h2>Not Featured Articles</h2>
                    @if(isset($articles['not_featured']))
                        @foreach ($articles['not_featured']->chunk(2) as $chunkedArticle)
                            <div class="row">
                                @foreach($chunkedArticle as $article)
                                    @if($article->featured != 'featured')
                                        <div class="col-md-4">
                                            <div class="card">
                                                <img class="card-img-top" alt="Bootstrap Thumbnail First"
                                                     width="200" src="{{url("images/$article->image")}}"/>
                                                <div class="card-block">
                                                    <h5 class="card-title">
                                                        {{$article->title}}
                                                    </h5>
                                                    <p class="card-text">
                                                        category: {{$article->categories->name}}
                                                    </p>
                                                    <p>
                                                        <a class="btn btn-primary" href="{{url("article/$article->id/")
                                }}">Details</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>