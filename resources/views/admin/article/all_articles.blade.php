@include('admin.nav')
<div class="container-fluid main-container">
    @include('admin.side_admin')
    @foreach ($articles->chunk(2) as $chunkedArticle)
        <div class="row">
            @foreach($chunkedArticle as $article)
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top" alt="Bootstrap Thumbnail First"
                             width="300" src="{{url("images/$article->image")}}"/>
                        <div class="card-block">
                            <h5 class="card-title">
                                {{$article->title}}
                            </h5>
                            <p class="card-text">
                                category: {{$article->categories->name}}
                            </p>
                            <p>
                                <a class="btn btn-primary" href="{{url("admin/articles/$article->id/edit")
                                }}">Edit</a>
                                <a class="btn" href="{{url("/admin/articles/$article->id/delete")}}">Delete</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>