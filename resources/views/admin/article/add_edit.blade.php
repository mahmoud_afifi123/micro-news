@include('admin.nav')
<script src="{{url('js/ckeditor_3/ckeditor.js')}}"></script>
<div class="container-fluid main-container">
    @include('admin.side_admin')
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Edit Article
            </div>
            <div class="panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" enctype="multipart/form-data"
                      @if(!isset($articleData))
                      action="{{route('articles.store')}}"
                      @else action="{{route('articles.update',$articleData->id)}}" @endif
                >
                    {{ csrf_field() }}
                    @if(isset($articleData))
                        {{ method_field('PATCH') }}
                    @endif
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" @if(isset($articleData))
                        value="{{$articleData->title}} @endif">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" id="editor1" rows="10" cols="80">
                            @if(isset($articleData))
                                {{$articleData->body}}
                            @endif
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>Publish</label><br>
                        <input type="radio" name="publish[]" @if(isset($articleData) and $articleData->published == 1) checked @endif
                               value="1">publish<br>
                        <input type="radio" name="publish[]" @if(isset($articleData) and $articleData->published == 0) checked @endif
                        value="0">un publish
                    </div>
                    <div class="form-group">
                        <label>Featured</label><br>
                        <input type="radio" name="featured[]" @if(isset($articleData) and $articleData->featured ==
                        'featured') checked @endif
                        value="featured">Featured<br>
                        <input type="radio" name="featured[]" @if(isset($articleData) and $articleData->featured == 'not_featured') checked
                               @endif
                        value="not_featured">not Featured
                    </div>
                    <div class="form-group">
                        <label>image</label><br>
                        <input type="file" name="image">
                    </div>
                    <div class="form-group">
                        <label>Enter order(priority) of article in number</label><br>
                        <input type="number" @if(isset($articleData->order)) value="{{$articleData->order}}" @endif
                               class="form-control"
                               name="priority"><br>
                    </div>
                    <div class="form-group">
                        <label>Category</label><br>
                        <select name="category" class="form-control">
                            @foreach($categories as $category)
                                <option @if(isset($articleData) and $articleData->category_id == $category->id)
                                        selected
                                        @endif
                                value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" @if(isset($articleData)) value="Update" @else
                        value="save" @endif>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace("editor1");
</script>
