@include('admin.nav')
<div class="container-fluid main-container">
    @include('admin.side_admin')
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dashboard
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="hero-widget well well-sm">
                            <div class="text">
                                <label style="font-size: 50px;" class="text-muted">{{$articlesCount}} Articles</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="hero-widget well well-sm">
                            <div class="text">
                                <label style="font-size: 50px;" class="text-muted">{{$featuredArticlesCount}}
                                    Featured Articles</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="hero-widget well well-sm">
                            <div class="text">
                                <label style="font-size: 50px;" class="text-muted">{{$usersCount}} Users</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>