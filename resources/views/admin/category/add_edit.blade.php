@include('admin.nav')
<div class="container-fluid main-container">
    @include('admin.side_admin')
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Edit Category
            </div>
            <div class="panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST"
                      @if(!isset($categoryData))
                      action="{{route('categories.store')}}"
                      @else action="{{route('categories.update',$categoryData->id)}}" @endif
                >
                    {{ csrf_field() }}
                    @if(isset($categoryData))
                        {{ method_field('PATCH') }}
                    @endif
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" @if(isset($categoryData->name))
                        value="{{$categoryData->name}}" @endif
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" @if(!isset($categoryData)) value="Save"
                               @else value="update" @endif class="form-control">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>