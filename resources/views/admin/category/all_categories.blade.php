@include('admin.nav')
<div class="container-fluid main-container">
    @include('admin.side_admin')
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                All Categories
            </div>
            <div class="panel-body">
                @foreach ($categories as $category)
                    <div class="col-md-8">
                        {{ $category->name }}
                    </div>
                    <div class="col-md-4">
                        <a href="{{url("/admin/categories/$category->id/edit")}}" class="btn btn-info">Edit</a>
                        <a href="{{url("/admin/categories/$category->id/delete")}}" class="btn btn-danger">Delete</a>
                    </div>
                @endforeach
                {{ $categories->links() }}
            </div>
        </div>
    </div>
</div>