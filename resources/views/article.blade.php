@include('admin.nav')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <img style="margin-left: 400px;" alt="Bootstrap Image Preview" width="400" src="{{url
            ("images/$article->image")}}"/>
            <h3>
                <strong>Category: </strong>{{$article->categories->name}}</small>
            </h3>
            <p>
                {{$article->body}}
            </p>
        </div>
    </div>
</div>