<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('categories')->orderBy('order', 'asc')->
        orderBy('id', 'desc')->get();
        return view('admin/article/all_articles', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin/article/add_edit', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'publish' => 'required',
            'featured' => 'required',
            'image' => 'required',
            'priority' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/articles/create')
                ->withErrors($validator)
                ->withInput();
        }
        $articleInstance = new Article;
        $articleInstance->title = $request->title;
        $articleInstance->body = $request->description;
        $articleInstance->published = $request->publish[0];
        $articleInstance->featured = $request->featured[0];
        //image sector
        $file = $request->file('image');
        $articleInstance->image = $file->getClientOriginalName();
        $file->move('images', $file->getClientOriginalName());
        //
        $articleInstance->order = $request->priority;
        $articleInstance->created_by = Auth::user()->id;
        $articleInstance->category_id = $request->category;
        if ($articleInstance->save()) {
            return redirect('admin/articles');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with('categories')->find($id);
        return view('article', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $articleData = Article::find($id);
        return view('admin/article/add_edit', ['categories' => $categories, 'articleData' => $articleData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'publish' => 'required',
            'featured' => 'required',
            'priority' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/articles/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }
        $articleInstance = Article::find($id);
        $articleInstance->title = $request->title;
        $articleInstance->body = $request->description;
        $articleInstance->published = $request->publish[0];
        $articleInstance->featured = $request->featured[0];
        //image sector
        if ($request->has('image')) {
            $file = $request->file('image');
            $articleInstance->image = $file->getClientOriginalName();
            $file->move('images', $file->getClientOriginalName());
        }
        //
        $articleInstance->order = $request->priority;
        $articleInstance->created_by = Auth::user()->id;
        $articleInstance->category_id = $request->category;
        if ($articleInstance->update()) {
            return redirect('admin/articles');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Article::find($id)->delete()) {
            return redirect('/admin/articles');
        }
    }
}
