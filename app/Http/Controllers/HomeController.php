<?php

namespace App\Http\Controllers;

use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Article::with('categories')->where('published', '=', 1)->orderBy('order', 'asc')->
        orderBy('id', 'desc')->get()->groupBy('featured');
        $title = 'micro-news';
        return view('home_page', ['articles' => $articles, 'title' => $title]);
    }
}
