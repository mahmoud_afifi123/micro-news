<?php

namespace App\Http\Controllers;

use App\Article;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articlesCount = Article::count();
        $featuredArticlesCount = Article::where('featured', 'featured')->count();
        $usersCount = Article::count();
        return view('admin.home_page', ['articlesCount' => $articlesCount, 'usersCount' => $usersCount, 'featuredArticlesCount' => $featuredArticlesCount]);
    }


}
